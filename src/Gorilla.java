import java.util.HashMap;
import java.util.Scanner;

public class Gorilla {

	char[] ALPHABET = "ARNDCQEGHILKMFPSTWYVBZX".toCharArray();
	HashMap<Character, Integer> char2id = new HashMap<Character, Integer>();
	int[][] score;
	char[] s2, s1;

	int blosum[][] = { { 4, -1, -2, -2, 0, -1, -1, 0, -2, -1, -1, -1, -1, -2, -1, 1, 0, -3, -2, 0, -2, -1, 0 },
			{ -1, 5, 0, -2, -3, 1, 0, -2, 0, -3, -2, 2, -1, -3, -2, -1, -1, -3, -2, -3, -1, 0, -1 },
			{ -2, 0, 6, 1, -3, 0, 0, 0, 1, -3, -3, 0, -2, -3, -2, 1, 0, -4, -2, -3, 3, 0, -1 },
			{ -2, -2, 1, 6, -3, 0, 2, -1, -1, -3, -4, -1, -3, -3, -1, 0, -1, -4, -3, -3, 4, 1, -1 },
			{ 0, -3, -3, -3, 9, -3, -4, -3, -3, -1, -1, -3, -1, -2, -3, -1, -1, -2, -2, -1, -3, -3, -2 },
			{ -1, 1, 0, 0, -3, 5, 2, -2, 0, -3, -2, 1, 0, -3, -1, 0, -1, -2, -1, -2, 0, 3, -1 },
			{ -1, 0, 0, 2, -4, 2, 5, -2, 0, -3, -3, 1, -2, -3, -1, 0, -1, -3, -2, -2, 1, 4, -1 },
			{ 0, -2, 0, -1, -3, -2, -2, 6, -2, -4, -4, -2, -3, -3, -2, 0, -2, -2, -3, -3, -1, -2, -1, },
			{ -2, 0, 1, -1, -3, 0, 0, -2, 8, -3, -3, -1, -2, -1, -2, -1, -2, -2, 2, -3, 0, 0, -1 },
			{ -1, -3, -3, -3, -1, -3, -3, -4, -3, 4, 2, -3, 1, 0, -3, -2, -1, -3, -1, 3, -3, -3, -1 },
			{ -1, -2, -3, -4, -1, -2, -3, -4, -3, 2, 4, -2, 2, 0, -3, -2, -1, -2, -1, 1, -4, -3, -1 },
			{ -1, 2, 0, -1, -3, 1, 1, -2, -1, -3, -2, 5, -1, -3, -1, 0, -1, -3, -2, -2, 0, 1, -1 },
			{ -1, -1, -2, -3, -1, 0, -2, -3, -2, 1, 2, -1, 5, 0, -2, -1, -1, -1, -1, 1, -3, -1, -1 },
			{ -2, -3, -3, -3, -2, -3, -3, -3, -1, 0, 0, -3, 0, 6, -4, -2, -2, 1, 3, -1, -3, -3, -1 },
			{ -1, -2, -2, -1, -3, -1, -1, -2, -2, -3, -3, -1, -2, -4, 7, -1, -1, -4, -3, -2, -2, -1, -2 },
			{ 1, -1, 1, 0, -1, 0, 0, 0, -1, -2, -2, 0, -1, -2, -1, 4, 1, -3, -2, -2, 0, 0, 0 },
			{ 0, -1, 0, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -2, -1, 1, 5, -2, -2, 0, -1, -1, 0 },
			{ -3, -3, -4, -4, -2, -2, -3, -2, -2, -3, -2, -3, -1, 1, -4, -3, -2, 11, 2, -3, -4, -3, -2 },
			{ -2, -2, -2, -3, -2, -1, -2, -3, 2, -1, -1, -2, -1, 3, -3, -2, -2, 2, 7, -1, -3, -2, -1 },
			{ 0, -3, -3, -3, -1, -2, -2, -3, -3, 3, 1, -2, 1, -1, -2, -2, 0, -3, -1, 4, -3, -2, -1 },
			{ -2, -1, 3, 4, -3, 0, 1, -1, 0, -3, -4, 0, -3, -3, -2, 0, -1, -4, -3, -3, 4, 1, -1 },
			{ -1, 0, 0, 1, -3, 3, 4, -2, 0, -3, -3, 1, -1, -3, -1, 0, -1, -3, -2, -2, 1, 4, -1 },
			{ 0, -1, -1, -1, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -2, 0, 0, -2, -1, -1, -1, -1, -1 } };

	public Gorilla() {
		for (int i = 0; i < ALPHABET.length; i++)
			char2id.put(ALPHABET[i], i);
	}

	public void execute(String st1, String st2) {
		s1 = st1.toCharArray();
		s2 = st2.toCharArray();
		score = new int[s1.length + 1][s2.length + 1];
		for (int i = 0; i < score.length; i++) {
			for (int j = 0; j < score[0].length; j++) {
				score[i][j] = Integer.MIN_VALUE;
			}
		}
		fillMatrix(s1.length, s2.length);
	}

	private int fillMatrix(int s1i, int s2i) {
		if (score[s1i][s2i] != Integer.MIN_VALUE) {
			return score[s1i][s2i];
		}
		if (s1i == 0) {
			score[0][s2i] = s2i * -4;
			return s2i * -4;
		}
		if (s2i == 0) {
			score[s1i][0] = s1i * -4;
			return s1i * -4;
		}
		score[s1i][s2i] = Math.max(fillMatrix(s1i - 1, s2i - 1) + getCharScore(s1[s1i - 1], s2[s2i - 1]),
				Math.max(fillMatrix(s1i - 1, s2i) - 4, fillMatrix(s1i, s2i - 1) - 4));
		return score[s1i][s2i];
	}

	public void traceBack(StringBuilder a1, StringBuilder a2) {
		System.out.println(traceBack(a1, a2, s1.length, s2.length));
	}

	private int traceBack(StringBuilder a1, StringBuilder a2, int i, int j) {

		while (i != 0 && j != 0) {
			int v1 = score[i - 1][j - 1] + getCharScore(s1[i - 1], s2[j - 1]);
			int v2 = score[i - 1][j] - 4;
			int v3 = score[i][j - 1] - 4;
			if (v1 > v2) {
				j--;
				if (v1 > v3) {
					i--;
					a1.append(s1[i]);
					a2.append(s2[j]);
				} else if (v3 > v1) {
					a1.append("-");
					a2.append(s2[j]);
				} else {
					StringBuilder a11 = new StringBuilder(a1.toString());
					StringBuilder a12 = new StringBuilder(a1.toString());
					StringBuilder a21 = new StringBuilder(a2.toString());
					StringBuilder a22 = new StringBuilder(a2.toString());
					a11.append(s1[i - 1]);
					a21.append(s2[j]);
					traceBack(a11, a21, i - 1, j);
					a12.append("-");
					a22.append(s2[j]);
					traceBack(a12, a22, i, j);
					int temp1 = getStringScore(a11.toString(), a21.toString());
					int temp2 = getStringScore(a12.toString(), a22.toString());
					if (temp1 > temp2) {
						a1.delete(0, a1.length());
						a1.append(a11.toString());
						a2.delete(0, a2.length());
						a2.append(a21.toString());
						return temp1;
					}
					a1.delete(0, a1.length());
					a1.append(a12.toString());
					a2.delete(0, a2.length());
					a2.append(a22.toString());
					return temp2;
				}
			} else if (v2 > v1) {
				if (v2 > v3) {
					i--;
					a1.append(s1[i]);
					a2.append("-");
				} else if (v2 < v3) {
					j--;
					a1.append("-");
					a2.append(s2[j]);
				} else {
					StringBuilder a11 = new StringBuilder(a1.toString());
					StringBuilder a12 = new StringBuilder(a1.toString());
					StringBuilder a21 = new StringBuilder(a2.toString());
					StringBuilder a22 = new StringBuilder(a2.toString());
					a11.append(s1[i - 1]);
					a21.append("-");
					traceBack(a11, a21, i - 1, j);
					a12.append("-");
					a22.append(s2[j - 1]);
					traceBack(a12, a22, i, j - 1);
					int temp1 = getStringScore(a11.toString(), a21.toString());
					int temp2 = getStringScore(a12.toString(), a22.toString());
					if (temp1 > temp2) {
						a1.delete(0, a1.length());
						a1.append(a11.toString());
						a2.delete(0, a2.length());
						a2.append(a21.toString());
						return temp1;
					}
					a1.delete(0, a1.length());
					a1.append(a12.toString());
					a2.delete(0, a2.length());
					a2.append(a22.toString());
					return temp2;
				}
			} else {
				StringBuilder a11 = new StringBuilder(a1.toString());
				StringBuilder a12 = new StringBuilder(a1.toString());
				StringBuilder a21 = new StringBuilder(a2.toString());
				StringBuilder a22 = new StringBuilder(a2.toString());
				a11.append(s1[i - 1]);
				a21.append(s2[j - 1]);
				traceBack(a11, a21, i - 1, j - 1);
				a12.append(s1[i - 1]);
				a22.append("-");
				traceBack(a12, a22, i - 1, j);
				int temp1 = getStringScore(a11.toString(), a21.toString());
				int temp2 = getStringScore(a12.toString(), a22.toString());
				if (temp1 > temp2) {
					a1.delete(0, a1.length());
					a1.append(a11.toString());
					a2.delete(0, a2.length());
					a2.append(a21.toString());
					return temp1;
				}
				a1.delete(0, a1.length());
				a1.append(a12.toString());
				a2.delete(0, a2.length());
				a2.append(a22.toString());
				return temp2;
			}
		}
		if (i > 0) {
			while (i > 0) {
				i--;
				a1.append(s1[i]);
				a2.append("-");
			}
		}
		if (j > 0) {
			while (j > 0) {
				j--;
				a1.append("-");
				a2.append(s2[j]);
			}
		}
		a1.reverse();
		a2.reverse();
		return getStringScore(a1.toString(), a2.toString());
	}

	private int getCharScore(char c1, char c2) {
		return blosum[char2id.get(c1)][char2id.get(c2)];
	}

	private int getStringScore(String st1, String st2) {
		char[] temp1 = st1.toCharArray();
		char[] temp2 = st2.toCharArray();

		int sum = 0;
		for (int i = 0; i < temp1.length; i++) {
			char c1 = temp1[i];
			char c2 = temp2[i];
			if (c1 == '-' || c2 == '-') {
				sum -= 4;
			} else {
				sum += blosum[char2id.get(c1)][char2id.get(c2)];
			}
		}
		return sum;
	}

	public static void main(String[] args) {

		HashMap<Integer, String> nameMap = new HashMap<Integer, String>();

		Gorilla g = new Gorilla();
		Scanner sc = new Scanner(System.in);
		StringBuilder sb1, sb2;

		String temp = sc.nextLine();
		int index = temp.indexOf(" ");
		int n = Integer.parseInt(temp.substring(0, index));
		int q = Integer.parseInt(temp.substring(index + 1));

		for (int i = 0; i < n; i++) {
			nameMap.put(sc.nextLine().hashCode(), sc.nextLine());
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < q; i++) {
			temp = sc.nextLine();
			index = temp.indexOf(" ");
			g.execute(nameMap.get(temp.substring(0, index).hashCode()),
					nameMap.get(temp.substring(index + 1).hashCode()));
			sb1 = new StringBuilder();
			sb2 = new StringBuilder();
			g.traceBack(sb1, sb2);
			sb.append(sb1.toString() + "\n" + sb2.toString());
			if (i != q - 1)
				sb.append("\n");
		}
		sc.close();
		System.out.println(sb.toString());
	}
}
